# Generated by Django 4.0.3 on 2023-05-09 23:21

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('presentations', '0002_presentation_buns'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='presentation',
            name='buns',
        ),
    ]
